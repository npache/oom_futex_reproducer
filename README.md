# oom_futex_reproducer

This program can be used to reproduce a bug in the linux kernel robust futex facility.
When multiple processes are contending for a futex-backed lock on the robust_list (in our case,
a pthread_mutex with PTHREAD_MUTEX_ROBUST set via pthread_mutexattr) and the process
holding the lock is oom killed, we observe an apparent race between the oom_reaper kthread
and the process exit path whereby the userspace memory backing a contended futex is yanked
out from under the process before the exit path completes exit_robust_list. When this occurs,
the kernel never wakes up the waiters on this futex, causing them to hang indefinitely.

This reproducer demonstrates a case with a single waiter and a single lock.

Instructions for a system with a single cpu:
1. Run `make` to create the binary
2. Run `oom_futex_reproducer &`
3. After a few seconds, run `./check.sh`

Alternatively, simply run `./uberfast.sh`

The result of the reproducer is printed to the terminal.


Requirements (automated with setup.sh):
1. Force legacy cgroup hierarchy (this is one way to do it with systemd):
`grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"`

2. Install gdb


Outputs:

This reproducer may output two text files:
1. master_pid: this is the process ID of the top level parent that will
end up hung waiting on the lock. The file is used by `./check.sh`

2. log.txt contains the `dmesg` line that confirms that the alleged owner of the lock
waited on by process `cat master_pid` was indeed killed by the oom-killer


Note: The bug seems to reproduce far more consistently (apparently even deterministically) when the machine has 4 or fewer cpu cores.
