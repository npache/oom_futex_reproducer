#!/bin/bash

# This script forces systemd to mount the legacy cgroup hierarchy only

grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"
