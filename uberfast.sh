#!/bin/bash
# the fastest way (probably only usable on cpus <= 4 maximum)

BIN=oom_futex_reproducer

make clean;
make

./${BIN} & 

sleep 3
./check.sh

sleep 1;
pkill `echo ${BIN} | cut -c 1-15`

